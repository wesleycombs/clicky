package com.example.wcombs.buttontextchanger;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /* Declare counter variable */
    private static int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /* Reference the textView */
        final TextView tvNumClicks = (TextView) findViewById(R.id.tvNumClicks);
        final TextView tvTimes = (TextView) findViewById(R.id.tvTimes);
        /* Reference the button */
        Button mainButton = (Button) findViewById(R.id.btnClickMe);

        assert tvNumClicks != null;
        tvNumClicks.setText("" + counter);
        /* Increment the counter on click */
        assert mainButton != null;
        mainButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                counter++;
                tvNumClicks.setText(""+counter);

                /* if else to change times to time if counter==1 */
                if (counter ==1){
                    assert tvTimes != null;
                    tvTimes.setText(getResources().getString(R.string.time));
                } else {
                    assert tvTimes != null;
                    tvTimes.setText(getResources().getString(R.string.times));
                }
            }
         });


        /* Reset counter on long click */
        mainButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                counter=0;
                tvNumClicks.setText(""+counter);
                assert tvTimes != null;
                tvTimes.setText(getResources().getString(R.string.times));
                return true;
            }
        });
    }
}
